FROM nvidia/cuda:11.0.3-base-ubuntu20.04

RUN    DEBIAN_FRONTEND=noninteractive apt-get -y update \
    && DEBIAN_FRONTEND=noninteractive apt-get -y install python3 python3-pip \
    && pip install transformers torch

WORKDIR /app

COPY run.py /app

CMD python3 run.py
